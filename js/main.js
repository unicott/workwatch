$(document).ready(function(){
    var now;
    var timer;

    $('.startBtn').live('click', function() {
        if (!$(this).hasClass('inactive')) {
            $('.startBtn').addClass('inactive');
            $('.stopBtn').removeClass('inactive');
            startWorkWatch();
        }
    });

    $('.stopBtn').live('click', function() {
        if (!$(this).hasClass('inactive')) {
            $('.stopBtn').addClass('inactive');
            $('.startBtn').removeClass('inactive');
            stopWorkWatch();
        }
    });

    function startWorkWatch() {
        now = moment();
        timer = setInterval(startTimer, 500);
    }

    function startTimer() {
        if (now != null) {
//            var diff = moment(now).fromNow();
            var current = moment();//.format('h:mm:ss');
            var diff = current.diff(now);
//            var diff = current.fromNow(now);
            var result = moment(diff);
//            alert(result.format('HH:mm:ss'));
            result.utc();
//            alert(result.format('HH:mm:ss'));
//            result.subtract('h', 1);
            $('#stopWatch').html(result.format('HH:mm:ss'));
        }
    }

    function stopWorkWatch() {
        now = null;
        clearInterval(timer);
    }
});