### HEAD

### 1.1 (06 September, 2012)

* Fixed bug with time zones

### 1.0 (06 September, 2012)

* General look & layout
* Added moment.js
* Main timing functions