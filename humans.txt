# humanstxt.org/
# The humans responsible & technology colophon

# TEAM

    <name> -- <role> -- <twitter>
    Andrey Kuroedov -- Lead Developer - unicott

# THANKS

    <name>

# TECHNOLOGY COLOPHON

    HTML5, CSS3
    jQuery, Modernizr, Moment.js
